import Axios from "axios";
import { USER_MANGER } from "../constants/ModelManager";
export const addUserAction = (value, token) => {
  return async () => {
    try {
      const result = await Axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThemNguoiDung`,
        method: "POST",
        data: value,
        headers: {
          Authorization: "Bearer  " + token,
        },
      });
      alert("Thêm Người Dùng Thành Công");
      localStorage.setItem(USER_MANGER, JSON.stringify(result.data));
    } catch (err) {
      console.log(err.response?.data);
    }
  };
};
export const deleteManaAction = (taiKhoan, token) => {
  return async () => {
    try {
      const result = await Axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}
          `,
        method: "DELETE",
        headers: {
          Authorization: "Bearer  " + token,
        },
      });
      console.log(result);
      alert("Xóa Thành Công");
    } catch (err) {
      alert(err.response?.data);
    }
  };
};
