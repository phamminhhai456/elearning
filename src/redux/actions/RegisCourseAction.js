import axios from "axios";
import { USER_LOGIN } from "../constants/UserEducation";
import { getAPICourseAction } from "../constants/RegisCourseContant";

export const registionCoures = (maKhoaHoc, taiKhoan, token) => {
  return async (dispatch) => {
    try {
      const result = await axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/DangKyKhoaHoc`,
        method: "POST",
        data: {
          maKhoaHoc,
          taiKhoan,
        },
        headers: {
          Authorization: "Bearer  " + token,
        },
      });
      alert("Đăng Ký Khóa Học Thành Công");
    } catch (err) {
      alert(err.response?.data);
    }
  };
};
export const cancelCouresAPI = (maKhoaHoc, taiKhoan, token) => {
  return async () => {
    try {
      const result = await axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/HuyGhiDanh`,
        method: "POST",
        data: {
          maKhoaHoc,
          taiKhoan,
        },
        headers: {
          Authorization: "Bearer  " + token,
        },
      });

      alert("Hủy Khóa Học Thành Công");
    } catch (err) {
      alert(err.response?.data);
    }
  };
};

export const getRegisCouresAPI = (maNhom) => {
  return async (dispatch) => {
    try {
      const result = await axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=${maNhom}`,
        method: "GET",
      });
      dispatch({
        type: getAPICourseAction,
        coures: result.data,
      });
    } catch (err) {
      alert(err.response?.data);
    }
  };
};
export const getListCourseRegistedFormAPI = (couresIDYet, token) => {
  return async (dispatch) => {
    try {
      const result = await axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
        method: "POST",
        data: couresIDYet,
        headers: {
          Authorization: "Bearer  " + token,
        },
      });
      dispatch({ type: "DANH_SACH_DA_DANG_KY", data: result.data });
    } catch (err) {
      console.log(err.response?.data);
    }
  };
};
