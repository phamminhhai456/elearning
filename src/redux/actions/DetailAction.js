import Axios from "axios";
import { getDeatilAction } from "../constants/getAPIs";

export const getDetailAPI = (maKhoaHoc) => {
  return async (dispatch) => {
    try {
      const result = await Axios({
        url: `https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`,
        method: "GET",
      });
      dispatch({ type: getDeatilAction, couresDetail: result.data });
    } catch (err) {
      console.log(err.response?.data);
    }
  };
};
