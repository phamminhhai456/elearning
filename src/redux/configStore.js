import { applyMiddleware, combineReducers, createStore } from "redux";
import reduxThunk from "redux-thunk";
import { CourseReducer } from "./reducer/CourseReducer";
import { UserReducer } from "./reducer/UserReducer";
import { SearchUser } from "./reducer/SearchUser";
import { StuReducer } from "./reducer/StuReducer";
import { SearchCourseReducer } from "./reducer/SearchCourseReducer";
import { RegisCourseReducer } from "./reducer/RegisCourseReducer";
const rootReducer = combineReducers({
  CourseReducer,
  UserReducer,
  SearchUser,
  StuReducer,
  SearchCourseReducer,
  RegisCourseReducer,
});

export const store = createStore(rootReducer, applyMiddleware(reduxThunk));
