import { CHANGECOURESCONSTANT } from "../constants/ChangeCourseConstant";
import { getAPIAction, getDeatilAction, maKhoaHoc } from "../constants/getAPIs";
const initialState = {
  defaultID: "GP01",
  arrCoures: [],
  maKhocHoc: "",
  detailCoures: {},
  idOfCourse: "",
  arrListID: [],
  changeCoures: {},
};

export const CourseReducer = (state = initialState, action) => {
  switch (action.type) {
    case getAPIAction: {
      return { ...state, arrCoures: action.coures };
    }
    case maKhoaHoc: {
      return { ...state, maKhoaHoc: action.couresMKH };
    }
    case getDeatilAction: {
      state.detailCoures = action.couresDetail;
      return { ...state };
    }
    case "ID_COURES_ITEM": {
      return { ...state, defaultID: action.value };
    }
    case "LAY_MA_DANH_MUC": {
      return { ...state, arrListID: action.coures };
    }
    case CHANGECOURESCONSTANT: {
      return { ...state, changeCoures: action.coures };
    }

    default: {
      return state;
    }
  }
};
