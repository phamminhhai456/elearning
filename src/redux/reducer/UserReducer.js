import { logInAction } from "../constants/getAPIs";
import {
  ACCESS_TOKEN,
  INFOMATIONUSERREDUCER,
  USER_LOGIN,
} from "../constants/UserEducation";
let userLocalReal = "";

if (localStorage.getItem(USER_LOGIN)) {
  let userLocal = JSON.parse(localStorage.getItem(USER_LOGIN));
  userLocalReal = userLocal;
}

const initialState = {
  userLogin: userLocalReal,
  userInfo: [],
};

export const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case INFOMATIONUSERREDUCER: {
      return { ...state, userInfo: action.data };
    }
    default:
      return state;
  }
};
