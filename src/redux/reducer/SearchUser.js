import { SEARCH_USER } from "../constants/ModelManager";

const initialState = {
  arrUser: [],
};
export const SearchUser = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_USER: {
      return { ...state, arrUser: action.data };
    }
    default:
      return state;
  }
};
