import React from "react";
import CourseItem from "../../components/CourseItem/CourseItem";
import "../../scss/Background.scss";
export default function HomePage() {
  return (
    <div id="backGroundKNT">
      <div className="container">
        <CourseItem />
      </div>
    </div>
  );
}
