import { Field, Form, Formik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import * as Yup from "yup";
import { deleteManaAction } from "../../redux/actions/ManageUserAction";
import { ACCESS_TOKEN } from "../../redux/constants/UserEducation";
export default function DeleteUser(props) {
  const { userLogin } = useSelector((state) => state.UserReducer);
  let tokenLocal = JSON.parse(localStorage.getItem(ACCESS_TOKEN));
  console.log(tokenLocal);
  const SignupSchema = Yup.object().shape({
    taiKhoan: Yup.string().required("Mời Điền Tài Khoản Muốn Xóa"),
  });
  let ivalid = false;
  const dispatch = useDispatch();
  if (userLogin.maLoaiNguoiDung === "GV") {
    return (
      <div className="container">
        <div className="row flex-column align-items-center justify-content-center mt-5">
          <h3>Xóa Người Dùng</h3>
          <div className="col-6">
            <div className="form-group">
              <Formik
                initialValues={{
                  taiKhoan: "",
                }}
                validationSchema={SignupSchema}
                onSubmit={(values, { resetForm }) => {
                  dispatch(deleteManaAction(values.taiKhoan, tokenLocal));
                  resetForm({ values: "" });
                }}
              >
                {({ errors, touched, values }) => (
                  <Form>
                    <Field className="form-control" name="taiKhoan" />
                    {errors.taiKhoan && touched.taiKhoan ? (
                      <div className="alert alert-danger text-left">
                        {errors.taiKhoan}
                      </div>
                    ) : null}
                    <div className="text-center mt-3">
                      <button className="btn buttonDeleteUser" type="submit">
                        <span>Xóa Người Dùng</span>
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    alert("Bạn Phải Đăng Nhập Và Là Giáo Viên Mới Có Thể Truy Cập Trang Này");
    return <Redirect to="/login" />;
  }
}
