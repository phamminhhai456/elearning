import { Tabs } from "antd";
import TabPane from "antd/es/tabs/TabPane";
import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AddCourse from "../../components/ManageCourse/AddCourse";
import DeleteCourse from "../../components/ManageCourse/DeleteCourse";
import SearchCourse from "../../components/ManageCourse/SearchCourse";
import StuRegisYet from "../../components/StuManage/StuRegisYet";
import InfoUser from "../../components/UserManage/InfoUser";
import SearchUser from "../../components/UserManage/SearchUser";
import UpdateInfoUser from "../../components/UserManage/UpdateInfoUser";

export default function EditUser() {
  const { userLogin } = useSelector((state) => state.UserReducer);
  if (userLogin.maLoaiNguoiDung === undefined) {
    alert("Bạn Phải Đăng Nhập Mới Có Thể Truy Cập Trang Này!");
    return <Redirect to="/login" />;
  } else {
    return (
      <div className="container myTab">
        <Tabs defaultActiveKey="1">
          <TabPane tab="Cập Nhập Thông Tin" key="1">
            <UpdateInfoUser />
          </TabPane>
          <TabPane tab="Thông Tin Cá Nhân" key="3">
            <InfoUser />
          </TabPane>
          {userLogin.maLoaiNguoiDung === "GV" ? (
            <TabPane tab="Tìm Kiếm Người Dùng" key="4">
              <SearchUser />
            </TabPane>
          ) : (
            <div></div>
          )}
          {userLogin.maLoaiNguoiDung === "GV" ? (
            <TabPane tab="Thêm Khóa Học" key="5">
              <AddCourse />
            </TabPane>
          ) : (
            <div></div>
          )}
          {userLogin.maLoaiNguoiDung === "GV" ? (
            <TabPane tab="Xoá Khóa Học" key="6">
              <DeleteCourse />
            </TabPane>
          ) : (
            <div></div>
          )}
          {userLogin.maLoaiNguoiDung === "GV" ? (
            <TabPane tab="Tìm Học Viên" key="7">
              <StuRegisYet />
            </TabPane>
          ) : (
            <div></div>
          )}
          {userLogin.maLoaiNguoiDung === "GV" ? (
            <TabPane tab="Tìm Khóa Học" key="8">
              <SearchCourse />
            </TabPane>
          ) : (
            <div></div>
          )}
        </Tabs>
      </div>
    );
  }
}
