import { createBrowserHistory } from "history";
import { BrowserRouter, Route, Router, Switch } from "react-router-dom";
import "./App.css";
import DetailCourse from "./components/DetailCourse/DetailCourse";
import AddCourse from "./components/ManageCourse/AddCourse";
import UserLogin from "./components/UserLogin/UserLogin";
import AddUser from "./pages/AddUser/AddUser";
import DeleteUser from "./pages/DeleteUser/DeleteUser";
import EditCourse from "./pages/EditCourse/EditCourse";
import EditUser from "./pages/EditUser/EditUser";
import HomePage from "./pages/Home/HomePage";
import RegisCourse from "./pages/RegisCourse/RegisCourse";
import Registration from "./pages/Registration/Registration";
import HeaderTeamplate from "./templates/HeaderTeamplate";
export const history = createBrowserHistory();
function App() {
  return (
    <BrowserRouter history={history}>
      <HeaderTeamplate path="/" component={HomePage} />
      <HeaderTeamplate path="/home" component={HomePage} />
      <HeaderTeamplate path="/login" component={UserLogin} />
      <HeaderTeamplate path="/detail/:id" component={DetailCourse} />
      <HeaderTeamplate path="/register" component={Registration} />
      <HeaderTeamplate path="/edit" component={EditUser} />
      <HeaderTeamplate path="/adduser" component={AddUser} />
      <HeaderTeamplate path="/deleteuser" component={DeleteUser} />
      <HeaderTeamplate path="/addcourse" component={AddCourse} />
      <HeaderTeamplate path="/editcourse" component={EditCourse} />
      <HeaderTeamplate path="/regiscourse" component={RegisCourse} />
    </BrowserRouter>
  );
}

export default App;
