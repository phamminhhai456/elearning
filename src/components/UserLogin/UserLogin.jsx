import { Button, Checkbox, Form, Input } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { userLoginAPI } from "../../redux/actions/UserLogin";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
export default function UserLogin() {
  const { userLogin } = useSelector((state) => state.UserReducer);
  const dispatch = useDispatch();
  let ivalid = true;
  const [userData, setUserData] = useState({ taiKhoan: "", matKhau: "" });
  const handleSubmit = (event) => {
    event.preventDefault();
  };
  const validationUser = (userData) => {
    if (userData.taiKhoan === "") {
      ivalid = false;
    } else if (userData.matKhau === "") {
      ivalid = false;
    }

    console.log(ivalid);
    return ivalid;
  };

  if (userLogin.maLoaiNguoiDung !== undefined) {
    return <Redirect to="/" />;
  } else {
    return (
      <Form
        className="container-fluid mt-5"
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        autoComplete="off"
      >
        <div className="row flex-column align-items-center justify-content-center">
          <div className="col-6">
            <h3 className="text-center">Đăng Nhập</h3>
            <Form.Item
              className="marinLoGin"
              onChange={(e) => {
                setUserData({ ...userData, taiKhoan: e.target.value });
              }}
              label="Tài Khoản"
              name="taiKhoan"
              rules={[
                { required: true, message: "Vui Lòng Điền Tên Tài Khoản" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className="marinLoGin"
              onChange={(e) => {
                setUserData({ ...userData, matKhau: e.target.value });
              }}
              label="Mật Khẩu"
              name="matKhau"
              rules={[{ required: true, message: "Vui Lòng Điền Mật Khẩu" }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button
                className="marinLoGin maxWith280"
                onClick={() => {
                  validationUser(userData);
                  if (ivalid) {
                    dispatch(userLoginAPI(userData));
                  }
                }}
                type="primary"
                htmlType="submit"
                danger
              >
                Đăng Nhập
              </Button>
            </Form.Item>
          </div>
        </div>
      </Form>
    );
  }
}
