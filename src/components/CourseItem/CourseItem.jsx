import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getCourseAPI } from "../../redux/actions/CourseItemAction";
import { ACCESS_TOKEN } from "../../redux/constants/UserEducation";
import { CHANGECOURESCONSTANT } from "../../redux/constants/ChangeCourseConstant";
export default function CourseItem() {
  let tokenLocal = JSON.parse(localStorage.getItem(ACCESS_TOKEN));
  const { userLogin } = useSelector((state) => state.UserReducer);
  const { arrCoures } = useSelector((state) => state.CourseReducer);
  const { defaultID } = useSelector((state) => state.CourseReducer);
  const dispatch = useDispatch();
  const renderCourse = () => {
    return arrCoures.map((course, index) => {
      return (
        <div key={index} className="col-12 col-xl-4 col-sm-6 mt-5 cardKNT ">
          <div className="contentOfCoures">
            <NavLink to={`/detail/${course.maKhoaHoc}`}>
              <div
                style={{
                  objectFit: "cover",
                  backgroundImage: `url("https://via.placeholder.com/500")`,
                }}
              >
                <img src={course.hinhAnh} alt="" />
              </div>
              <div className="textTitleKNT">
                <span>Tên Khóa Học: {course.tenKhoaHoc.substring(0, 30)}</span>
              </div>
            </NavLink>
            <div id="contenNormal" className="textContent">
              <span>Mã Khóa Học: </span>
              <span>{course.maKhoaHoc}</span>
              <div>
                <span>Lượt Xem: </span> <span>{course.luotXem}</span>
              </div>
              <div>
                <span>Số Học Viên: </span>
                <span>{course.soLuongHocVien}</span>
              </div>
              <div className="text-warning">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
              </div>
            </div>
          </div>

          <div className="row">
            {userLogin.maLoaiNguoiDung === "GV" ? (
              <NavLink
                to="editcoures"
                onClick={() => {
                  localStorage.setItem(
                    CHANGECOURESCONSTANT,
                    JSON.stringify(course)
                  );
                }}
                className="btn myButton editOfTeacher"
              >
                Sửa Khóa Học
              </NavLink>
            ) : (
              <div></div>
            )}
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    dispatch(getCourseAPI(defaultID));
  }, [defaultID]);
  return (
    <div className="textTitleOfHome">
      <div className="maginOfMe">
        <h1>Danh Sách Khóa Học</h1>
        <h3>Chọn Khóa Học Muốn Tìm Kiếm</h3>
      </div>

      <select
        id="textTitleInput"
        className="form-control"
        as="select"
        name="maNhom"
      >
        <option>GP01</option>
        <option>GP02</option>
        <option>GP03</option>
        <option>GP04</option>
        <option>GP05</option>
        <option>GP06</option>
        <option>GP07</option>
        <option>GP08</option>
        <option>GP09</option>
        <option>GP10</option>
        <option>GP11</option>
        <option>GP12</option>
        <option>GP13</option>
        <option>GP14</option>
        <option>GP15</option>
      </select>
      <div className="row">{renderCourse()}</div>
    </div>
  );
}
