import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import Header from "../components/Header/Header";

export default function HeaderTeamplate(props) {
  return (
    <Fragment>
      <Route
        path={props.path}
        exact
        render={(propsRoute) => {
          return (
            <Fragment>
              <Header />
              <div>
                <div>
                  <props.component {...propsRoute} />
                </div>
              </div>
            </Fragment>
          );
        }}
      />
    </Fragment>
  );
}
